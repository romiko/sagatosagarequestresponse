﻿using System;
using NServiceBus;
using SagaToSagaRequestResponse.Internal.Commands.Orders;
using NServiceBus.Saga;
using SagaToSagaRequestResponse.Internal.Messages.Orders;


namespace SagaToSagaRequestResponse.Orders
{
    public partial class ProcessOrderHandler
    {
        public override void ConfigureHowToFindSaga()
        {
            ConfigureMapping<ProcessOrder>(cmd => cmd.OrderId).ToSaga(data => data.OrderId);
            ConfigureMapping<ProcessOrderPaymentResponse>(cmd => cmd.OrderId).ToSaga(data => data.OrderId); //Romiko - Needed for ReplyToOriginator
            // If you add new messages to be handled by your saga, you will need to manually add a call to ConfigureMapping for them.
        }
    }
}
