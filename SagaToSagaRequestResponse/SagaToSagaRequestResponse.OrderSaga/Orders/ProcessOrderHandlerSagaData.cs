﻿using System;
using NServiceBus;
using SagaToSagaRequestResponse.Internal.Commands.Orders;
using NServiceBus.Saga;


namespace SagaToSagaRequestResponse.Orders
{
    public partial class ProcessOrderHandlerSagaData
    {
        [Unique]
        public string OrderId { get; set; }
    }
}
