﻿using System;
using NServiceBus;
using NServiceBus.Saga;
using SagaToSagaRequestResponse.Internal.Commands.Orders;
using SagaToSagaRequestResponse.Internal.Messages.Orders;


namespace SagaToSagaRequestResponse.Orders
{
    public partial class ProcessOrderHandler : IHandleTimeouts<TerminateProcessOrderHandler>
    {
        public TimeSpan TimeOut = TimeSpan.FromMinutes(1); //Romiko this must be greater than the total living time of the called saga (Payements)
        partial void HandleImplementation(ProcessOrder message)
        {
            RequestTimeout<TerminateProcessOrderHandler>(TimeOut);

            if (string.IsNullOrEmpty(Data.OrderId))
                Data.OrderId = message.OrderId;

            Console.WriteLine("Orders received {0}, Order Id: {1}, Amount {2} ",  
                message.GetType().Name, message.OrderId, message.Amount);

            var processOrderPayment = new ProcessOrderPayment
            {
                OrderId = message.OrderId,
                Amount = message.Amount
            };
            Bus.Send(processOrderPayment);
        }


        partial void HandleImplementation(ProcessOrderPaymentResponse message)
        {
            Console.WriteLine("Payment Response Received, Order Id: {0}, Success: {1}, SentByReplyToOriginator: {2}", 
                message.OrderId, message.PaymentSuccess, message.SentByReplyToOriginator);
        }

        /// <summary>
        /// Romiko - Needed for ReplyToOriginator to work, Calling Saga must outlive called Saga.
        /// </summary>
        /// <param name="state"></param>
        public void Timeout(TerminateProcessOrderHandler state)
        {
            MarkAsComplete();
        }
    }

    public class TerminateProcessOrderHandler { }
}
