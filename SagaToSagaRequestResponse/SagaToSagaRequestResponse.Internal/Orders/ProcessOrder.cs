﻿using System;

namespace SagaToSagaRequestResponse.Internal.Commands.Orders
{
    public class ProcessOrder
    {
        public string OrderId { get; set; }
        public decimal Amount { get; set; }
    }
}
