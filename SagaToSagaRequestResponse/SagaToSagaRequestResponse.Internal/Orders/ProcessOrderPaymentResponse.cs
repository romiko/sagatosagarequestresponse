﻿using System;

namespace SagaToSagaRequestResponse.Internal.Messages.Orders
{
    public class ProcessOrderPaymentResponse
    {
        public string OrderId { get; set; }
        public bool PaymentSuccess { get; set; }
        public bool SentByReplyToOriginator { get; set; }
    }
}
