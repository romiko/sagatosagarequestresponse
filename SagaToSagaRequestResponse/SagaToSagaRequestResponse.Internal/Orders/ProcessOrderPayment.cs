﻿using System;

namespace SagaToSagaRequestResponse.Internal.Commands.Orders
{
    public class ProcessOrderPayment
    {
        public string OrderId { get; set; }
        public decimal Amount { get; set; }
    }
}
