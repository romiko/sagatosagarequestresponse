﻿using System;
using NServiceBus;
using SagaToSagaRequestResponse.Internal.Commands.Orders;


namespace SagaToSagaRequestResponse.Orders
{
    public partial class ProcessOrderSender : IWantToRunWhenBusStartsAndStops
    {
        public void Start()
        {
            //Simulate some sort of listener that would receive orders from a web server and send orders
            //Listerner starts on port whatever and then we send the orders
            for (var i = 0; i < 3; i++)
            {
                var command = new ProcessOrder
                {
                    OrderId = string.Format("Ord-{0}", i), 
                    Amount = i*7
                };
                Bus.Send(command);
            }
        }

        public void Stop()
        {
        }
    }
}
