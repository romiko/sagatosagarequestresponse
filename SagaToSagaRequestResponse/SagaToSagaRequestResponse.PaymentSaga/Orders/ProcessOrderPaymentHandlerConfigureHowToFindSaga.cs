﻿using System;
using NServiceBus;
using SagaToSagaRequestResponse.Internal.Commands.Orders;
using NServiceBus.Saga;


namespace SagaToSagaRequestResponse.Orders
{
    public partial class ProcessOrderPaymentHandler
    {
        public override void ConfigureHowToFindSaga()
        {

            ConfigureMapping<ProcessOrderPayment>(cmd => cmd.OrderId).ToSaga(data => data.OrderId);
            
            // If you add new messages to be handled by your saga, you will need to manually add a call to ConfigureMapping for them.
        }
    }
}
