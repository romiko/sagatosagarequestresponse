﻿using System;
using System.Threading;
using NServiceBus;
using NServiceBus.Saga;
using SagaToSagaRequestResponse.Internal.Commands.Orders;
using SagaToSagaRequestResponse.Internal.Messages.Orders;


namespace SagaToSagaRequestResponse.Orders
{
    public partial class ProcessOrderPaymentHandler : IHandleTimeouts<PaymentTimeout>
    {

        public TimeSpan TimeOut = TimeSpan.FromSeconds(20); //This saga must always live a shorter life than the orginating Saga.
        partial void HandleImplementation(ProcessOrderPayment message)
        {

            RequestTimeout<PaymentTimeout>(TimeOut);

            if (string.IsNullOrEmpty(Data.OrderId))
            {Data.OrderId = message.OrderId;}

            Console.WriteLine("ProcessOrderPayment received {0}, Order Id: {1}, Amount {2} ",
                message.GetType().Name, message.OrderId, message.Amount);

            Thread.Sleep(TimeSpan.FromSeconds(1));
            var response = new ProcessOrderPaymentResponse()
            {
                OrderId = message.OrderId,
                PaymentSuccess = true
            };
           Bus.Reply(response);
        }


        /// <summary>
        /// Used to prove ReplyToOriginator works in a timeout
        /// </summary>
        /// <param name="state"></param>
        public void Timeout(PaymentTimeout state)
        {
            var message = new ProcessOrderPaymentResponse
            {
                OrderId = Data.OrderId,
                PaymentSuccess = true,
                SentByReplyToOriginator = true
            };

            ReplyToOriginator(message); //Calling Saga must be alive i.e. In Raven..So its timeout but be greater than this one.
            MarkAsComplete();
        }
    }

    public class PaymentTimeout { }
}
